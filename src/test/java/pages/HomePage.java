package pages;

import drivers.GoogleDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.IOException;

public class HomePage {
    ChromeDriver driver = GoogleDriverProvider.getDriver();
    public HomePage() throws IOException {
    }

    public boolean isOpen() {
        WebElement expected = driver.findElement(By.xpath("//div[@class='store_nav']"));
        return expected.isEnabled();
    }

    public void searchGames(String name) {
        WebElement search = driver.findElement(By.xpath("//div[@class='searchbox']/input[@id='store_nav_search_term']"));
        search.click();
        search.sendKeys(name);
        WebElement searchButton = driver.findElement(By.xpath("//div[@class='searchbox']//img"));
        searchButton.click();
    }
}
