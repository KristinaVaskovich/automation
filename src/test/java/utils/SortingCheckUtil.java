package utils;

import pages.SearchPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingCheckUtil {
    public static boolean check(int number) throws IOException {
        SearchPage page = new SearchPage();
        List<Integer> checkList;

        checkList = page.listOfSortedGames();
        List<Integer> listWith = checkList.subList(0, number);
        List<Integer> sortedList = new ArrayList<>(listWith);

        Collections.sort(sortedList, Collections.reverseOrder());

        return listWith.equals(sortedList);
    }
}
