package drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class GoogleDriverProvider {
    private static ChromeDriver driver = null;

    public static ChromeDriver getDriver() throws IOException {
        FileInputStream fis;
        Properties property = new Properties();
        fis = new FileInputStream("src/test/resources/config.properties");
        property.load(fis);

        String URL = property.getProperty("url");
        String ru_language = property.getProperty("ru_language");
        String es_language = property.getProperty("es_language");
        String en_language = property.getProperty("en_language");

        if (driver == null) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments(ru_language);
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(options);
            driver.get(URL);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void deleteCookies() {
        driver = null;
        driver.quit();
    }
}
