package tests;

import drivers.GoogleDriverProvider;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.HomePage;
import pages.SearchPage;
import utils.SortingCheckUtil;

public class SteamTest {
    @Test(dataProvider = "webSiteData")
    public void storeTest(String game, int number) throws Exception {
        HomePage page1 = new HomePage();
        Assert.assertTrue(page1.isOpen());

        page1.searchGames(game);
        SearchPage page2 = new SearchPage();
        Assert.assertTrue(page2.isPageWithResultsOpen());
        Assert.assertTrue(page2.isListOfResultFull());

        page2.sortGames();
        Assert.assertTrue(SortingCheckUtil.check(number));
        GoogleDriverProvider.deleteCookies();
    }

    @DataProvider(name = "webSiteData")
    public Object[][] getData() {
        return new Object[][]{{"Fallout", 20},
                {"The Witcher", 10}};
    }
}
