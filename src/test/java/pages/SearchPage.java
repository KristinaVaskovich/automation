package pages;

import drivers.GoogleDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchPage {
    ChromeDriver driver = GoogleDriverProvider.getDriver();
    private final Wait<WebDriver> wait = new WebDriverWait(driver, 5, 5000);

    public SearchPage() throws IOException {
    }

    public boolean isPageWithResultsOpen() {
        WebElement searchResult = driver.findElement(By.xpath("//div[@class='searchtag tag_dynamic']"));
        return searchResult.isEnabled();
    }

    public boolean isListOfResultFull() {
        List<WebElement> resultPictures = driver.findElements(By.xpath("//div[@class='col search_capsule']/img"));
        return resultPictures.size() > 0;
    }

    public void sortGames() {
        WebElement sortPanel = driver.findElement(By.xpath("//a[@id='sort_by_trigger']"));
        sortPanel.click();

        WebElement priceSortButton = driver.findElement(By.xpath("//a[@id='Price_DESC']"));
        priceSortButton.click();
    }

    public ArrayList<Integer> listOfSortedGames() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='search_result_container' and  not(contains(@style,'opacity'))]")));
        List<WebElement> sortListGames = driver.findElements(By.xpath("//div[@class='col search_price_discount_combined responsive_secondrow']"));
        ArrayList<Integer> prices = new ArrayList<>();

        for (WebElement game : sortListGames) {
            String price = game.getAttribute("data-price-final");
            prices.add(Integer.parseInt(price));
        }
        return prices;
    }
}
